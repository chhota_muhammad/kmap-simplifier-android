package com.example.chhota.kmapsimplifier;

import java.util.StringTokenizer;

public class LogicSimplification {

    String equ;
    StringTokenizer tok;
    String output;
    int a[];
    int i; int var;
    int arr[][];
    boolean cov[][];
    int row,column;
    boolean pos;
    String ab;
    public LogicSimplification(String equ){
        i=0;pos=false;
        output="";
        this.equ=equ;
        a=new int[15];
    }
    public LogicSimplification(){
        i=0;pos=false; output="";
    }
    public void sopPos(){
        if(equ.charAt(0)=='(')
            pos=true;
        if(pos)
            tok=new StringTokenizer(equ,".");
        else
            tok=new StringTokenizer(equ,"+");
    }
    public void seprate(){
        tok=new StringTokenizer(equ,",");
        while(tok.hasMoreTokens()){
            a[i]=Integer.parseInt(tok.nextToken());
            i++;
        }
    }
    public boolean convert(){
        String temp;

        boolean standard=true;
        int Notposition[]=new int[10];
        int ac[]=new int[4];
        temp=tok.nextToken();
        if(temp.charAt(0)=='(')
            temp=temp.substring(1,temp.length()-1);
        ab=temp;
        maxNumber();

        if(pos)
            tok=new StringTokenizer(equ,".");
        else
            tok=new StringTokenizer(equ,"+");
        while(tok.hasMoreTokens()){
            int variable=0;
            temp=tok.nextToken();
            if(temp.charAt(0)=='(')
                temp=temp.substring(1,temp.length()-1);
            System.out.println(temp);

            boolean notset=false;
            for(int j=temp.length()-1;j>=0;j--){
                char g=temp.charAt(j);
                int b=(int)g;
                if(b==39)
                    notset=true;
                if(b<=122 && b>=97 || b>=65 && b<=90){
                    variable++;
                    if(g=='a' || g=='A'){
                        if(notset)
                            ac[0]=0;
                        else
                            ac[0]=1;
                    }
                    if(g=='b' || g=='B'){
                        if(notset)
                            ac[1]=0;
                        else
                            ac[1]=1;
                    }
                    if(g=='c' || g=='C'){
                        if(notset)
                            ac[2]=0;
                        else
                            ac[2]=1;
                    }
                    if(g=='d' || g=='D'){
                        if(notset)
                            ac[3]=0;
                        else
                            ac[3]=1;
                    }
                    notset=false;
                }

            }
            if(var==4){
                a[i]=ac[0]*8+ac[1]*4+ac[2]*2+ac[3]*1;
                System.out.println("Values of a"+a[i]);i++;
                if(variable!=4)
                    standard=false;
            }
            else{
                a[i]=ac[0]*4+ac[1]*2+ac[2]*1+ac[3]*0;  i++;
                if(variable!=3)
                    standard=false;
            }
            System.out.println("Value"+ac[0]+" "+ac[1]+" "+ac[2]+" "+ac[3]);

        }
        return standard;
    }
    public void maxNumber(){

        int max=0;
        for(int i=0;i<ab.length();i++){
            char c=ab.charAt(i);
            int b=(int)c;
            if(b<=122 && b>=97 || b>=65 && b<=90)
                max++;
        }

        System.out.println("Max"+ab);
        if(max==4){
            row=4; column=4;
            arr=new int[column][row];
            cov=new boolean[column][row];
            for(int j=0;j<column;j++)
                for(int q=0;q<row;q++){
                    arr[j][q]=0;
                    cov[j][q]=false;
                }
            var=4;
        }
        else{
            row=4; column=2;
            arr=new int[column][row];
            cov=new boolean[column][row];
            for(int j=0;j<column;j++)
                for(int q=0;q<row;q++){
                    arr[j][q]=0;
                    cov[j][q]=false;
                }
            var=3;
        }
    }
    public boolean standardSopPos(){
        if(pos)
            tok=new StringTokenizer(equ,".");
        else
            tok=new StringTokenizer(equ,"+");
        while(tok.hasMoreTokens()){
            String temp=tok.nextToken();

        }
        return false;
    }
    public void arrayCopy(int col,int ro,int ar[][],boolean po){
        row=ro; column=col;  pos=po;
        if(col==4)
            var=4;
        else
            var=3;
        arr=new int[column][row];
        cov=new boolean[column][row];
        for(int j=0;j<column;j++)
            for(int q=0;q<row;q++){
                arr[j][q]=ar[j][q];
                cov[j][q]=false;
            }
    }
    public void eightVariable(){
        for(int j=0;j<i;j++)
            switch(a[j])
            {
                case 0: arr[0][0]=1;
                    break;
                case 1: arr[0][1]=1;
                    break;
                case 2: arr[0][3]=1;
                    break;
                case 3: arr[0][2]=1;
                    break;

                case 4: arr[1][0]=1;
                    break;
                case 5: arr[1][1]=1;
                    break;
                case 7: arr[1][2]=1;
                    break;
                case 6: arr[1][3]=1;
                    break;

                case 12: arr[2][0]=1;
                    break;
                case 13: arr[2][1]=1;
                    break;
                case 15: arr[2][2]=1;
                    break;
                case 14: arr[2][3]=1;
                    break;

                case 8: arr[3][0]=1;
                    break;
                case 9: arr[3][1]=1;
                    break;
                case 11: arr[3][2]=1;
                    break;
                case 10: arr[3][3]=1;
                    break;
            }
    }

    public void simplify(){
        if(var==3){
            for(int e=0;e<2;e++)
                groupofFourHori(e);
            for(int e=0;e<2;e++)
                for(int j=0;j<4;j++)
                    groupofFour(e,j);
            for(int e=0;e<2;e++)
                for(int j=0;j<4;j++)
                    groupofTwoHori(e,j);
            for(int e=0;e<2;e++)
                for(int j=0;j<4;j++)
                    groupofTwoVerti(e,j);
            for(int e=0;e<2;e++)
                for(int j=0;j<4;j++)
                    groupofOne(e,j);
        }
        else{
            for(int e=0;e<4;e++)
                groupofEighthori(e);
            for(int e=0;e<4;e++)
                groupofEightverti(e);
            for(int e=0;e<4;e++)
                groupofFourVerti(e);
            for(int e=0;e<4;e++)
                groupofFourHori(e);
            for(int e=0;e<4;e++)
                for(int j=0;j<4;j++)
                    groupofFour(e,j);
            for(int e=0;e<4;e++)
                for(int j=0;j<4;j++)
                    groupofTwoHori(e,j);
            for(int e=0;e<4;e++)
                for(int j=0;j<4;j++)
                    groupofTwoVerti(e,j);
            for(int e=0;e<4;e++)
                for(int j=0;j<4;j++)
                    groupofOne(e,j);
        }
        //printboolean();
    }

    public void groupofEighthori(int colno)
    {
        boolean set=true;
        boolean covB=false;
        for(int j=colno;j<=colno+1;j++)
            for(int q=0;q<row;q++) {
                if(j==column){
                    if(arr[0][q]==0){
                        set=false;
                        break;
                    }
                }
                else
                if(arr[j][q]==0){
                    set=false;
                    break;
                }
            }
        if(set){
            for(int j=colno;j<=colno+1;j++)
                for(int q=0;q<row;q++){
                    if(j==column){
                        if(cov[0][q]==false){
                            covB=true;
                            break;
                        }
                    }
                    else
                    if(cov[j][q]==false) {
                        covB=true;
                        break;
                    }
                }
        }
        System.out.println("set:"+set+ "cov:"+covB);
        if(covB){
            for(int j=colno;j<=colno+1;j++)
                for(int q=0;q<row;q++){
                    if(j==column)
                        cov[0][q]=true;
                    else
                        cov[j][q]=true;
                }
            addOutput("groupofEight",colno,colno);
            System.out.println("Group of eight Hori Selected :"+colno);
        }
    }
    public void groupofEightverti(int colno){
        boolean set=true;
        boolean covB=false;
        for(int j=0;j<column;j++)
            for(int q=colno;q<=colno+1;q++) {
                if(q==row){
                    if(arr[j][0]==0){
                        set=false;
                        break;
                    }
                }
                else
                    try{
                        if(arr[j][q]==0){
                            set=false;
                            break;
                        }}
                    catch(Exception e){
                        System.out.println(+j+" "+q);
                    }
            }
        if(set){
            for(int j=0;j<column;j++)
                for(int q=colno;q<=colno+1;q++){
                    if(q==row){
                        if(cov[j][0]==false){
                            covB=true;
                            break;
                        }
                    }
                    else
                    if(cov[j][q]==false) {
                        covB=true;
                        break;
                    }
                }
        }
        System.out.println("set:"+set+ "cov:"+covB);
        if(covB){
            for(int j=0;j<column;j++)
                for(int q=colno;q<=colno+1;q++){
                    if(q==row)
                        cov[j][0]=true;
                    else
                        cov[j][q]=true;
                }
            addOutput("groupofEightVerti",colno,colno);
            System.out.println("Group of eight Verti Selected :"+colno);
        }
    }
    public void groupofFourVerti(int colno)
    {
        boolean set=true;
        boolean covB=false;
        for(int j=0;j<column;j++){
            if(arr[j][colno]==0){
                set=false;
                break;
            }
        }
        if(set){
            for(int j=0;j<column;j++){
                if(cov[j][colno]==false)
                {
                    covB=true;
                    break;
                }
            }
        }

        if(covB){
            for(int j=0;j<column;j++){
                cov[j][colno]=true;
            }
            addOutput("vertiFour",5,colno);
            System.out.println("Column Selected is: "+colno);
        }
    }

    public void groupofFourHori(int colno)
    {
        boolean set=true;
        boolean covB=false;
        for(int j=0;j<row;j++){
            if(arr[colno][j]==0){
                set=false;
                break;
            }
        }
        if(set){
            for(int j=0;j<row;j++){
                if(cov[colno][j]==false)
                {
                    covB=true;
                    break;
                }
            }
        }

        if(covB){
            for(int j=0;j<row;j++){
                cov[colno][j]=true;
            }
            addOutput("HoriFour",colno,5);
            System.out.println("Row Selected is: "+colno);
        }
    }

    public void groupofFour(int colno,int rowno)
    {
        boolean set=true;
        boolean covB=false;
        for(int j=colno;j<=colno+1;j++)
            for(int q=rowno;q<=rowno+1;q++)
            {
                if(j==column || q==row)
                {
                    if(q==row && j==column)
                        if(arr[0][0]==0){
                            System.out.println("J: "+j+ " q :"+q);
                            set=false;
                            break;
                        }
                    if(j==column && q!=row)
                    {
                        // System.out.println("J: "+j+ " q :"+q);
                        if(arr[0][q]==0){
                            set=false;
                            break;
                        }
                    }

                    if(q==row && j!=column)
                        if(arr[j][0]==0){
                            set=false;
                            break;
                        }
                }
                else
                if(arr[j][q]==0){
                    set=false;
                    break;
                }
            }
        if(set)
        {
            for(int j=colno;j<=colno+1;j++)
                for(int q=rowno;q<=rowno+1;q++)
                {
                    if(j==column || q==row)
                    {
                        if(q==row && j==column)
                            if(cov[0][0]==false){
                                covB=true;
                                break;
                            }
                        if(j==column && q!=row)
                            if(cov[0][q]==false){
                                covB=true;
                                break;
                            }

                        if(q==row && j!=column)
                            if(cov[j][0]==false){
                                covB=true;
                                break;
                            }
                    }
                    else
                    {
                        if(cov[j][q]==false){
                            covB=true;
                            break;
                        }
                    }
                }
        }
        if(covB)
        {
            for(int j=colno;j<=colno+1;j++)
                for(int q=rowno;q<=rowno+1;q++)
                {
                    if(j==column || q==row)
                    {
                        if(q==row && j==column)
                            cov[0][0]=true;
                        if(j==column && q!=row)
                            cov[0][q]=true;
                        if(q==row && j!=column)
                            cov[j][0]=true;
                    }
                    else
                        cov[j][q]=true;
                }
            addOutput("groupfour",colno,rowno);
            System.out.println("Group of four is Selected column :"+colno+" row :"+rowno);
        }
    }

    public void groupofTwoHori(int colno,int rowno)
    {
        boolean covB=false;
        boolean vertiset=false;
        boolean horiset=false;
        if(rowno==row-1){
            if(arr[colno][rowno]==1 && arr[colno][0]==1)
                if(cov[colno][rowno]==false && cov[colno][0]==false){
                    cov[colno][rowno]=true;
                    cov[colno][0]=true;
                    horiset=true;
                    covB=true;
                    addOutput("HoriTwo",colno,rowno);
                    System.out.println("Pair of two Hori ColNo:"+colno+" Rowno: "+rowno);
                }

        }
        else {
            if(arr[colno][rowno]==1 && arr[colno][rowno+1]==1)
                if(cov[colno][rowno]==false && cov[colno][rowno+1]==false){
                    cov[colno][rowno]=true;
                    cov[colno][rowno+1]=true;
                    horiset=true;
                    addOutput("HoriTwo",colno,rowno);
                    System.out.println("Pair of two Hori ColNo:"+colno+" Rowno: "+rowno);
                }
        }
        if(horiset==false)
            if(rowno==row-1){
                if(colno==column-1){
                    if((arr[colno][0]==1 && arr[0][0]==1))
                        if(cov[colno][0]==false && cov[0][0]==false)
                            vertiset=true;
                    if((arr[colno][0]==1 && arr[colno-1][0]==1))
                        if(cov[colno][0]==false && cov[colno-1][0]==false)
                            vertiset=true;
                }
                else if(colno==0){
                    if((arr[colno][0]==1 && arr[column-1][0]==1))
                        if(cov[colno][0]==false && cov[column-1][0]==false)
                            vertiset=true;
                    if((arr[colno][0]==1 && arr[colno+1][0]==1))
                        if(cov[colno][0]==false && cov[colno+1][0]==false)
                            vertiset=true;
                }
                else{
                    if((arr[colno][0]==1 && arr[colno+1][0]==1))


                        if(cov[colno][0]==false && cov[colno+1][0]==false)
                            vertiset=true;
                    if((arr[colno][0]==1 && arr[colno-1][0]==1))
                        if(cov[colno][0]==false && cov[colno-1][0]==false)
                            vertiset=true;
                }
            }
            else{
                if(colno==column-1){
                    if((arr[colno][rowno+1]==1 && arr[0][rowno+1]==1))
                        if(cov[colno][rowno+1]==false && cov[0][rowno+1]==false)
                            vertiset=true;
                    if((arr[colno][rowno+1]==1 && arr[colno-1][rowno+1]==1))
                        if(cov[colno][rowno+1]==false && cov[colno-1][rowno+1]==false)
                            vertiset=true;
                }
                else if(colno==0){
                    if((arr[colno][rowno+1]==1 && arr[column-1][rowno+1]==1))
                        if(cov[colno][rowno+1]==false && cov[column-1][rowno+1]==false)
                            vertiset=true;
                    if((arr[colno][rowno+1]==1 && arr[colno+1][rowno+1]==1))
                        if(cov[colno][rowno+1]==false && cov[colno+1][rowno+1]==false)
                            vertiset=true;

                }
                else{
                    if((arr[colno][rowno+1]==1 && arr[colno+1][rowno+1]==1))
                        if(cov[colno][rowno+1]==false && cov[colno+1][rowno+1]==false)
                            vertiset=true;
                    if((arr[colno][rowno+1]==1 && arr[colno-1][rowno+1]==1))
                        if(cov[colno][rowno+1]==false && cov[colno-1][rowno+1]==false)
                            vertiset=true;

                }
            }

        if(vertiset==false){
            if(rowno==row-1){
                if(arr[colno][rowno]==1 && arr[colno][0]==1)
                    if(cov[colno][rowno]==false || cov[colno][0]==false){
                        cov[colno][rowno]=true;
                        cov[colno][0]=true;
                        covB=true;
                        addOutput("HoriTwo",colno,rowno);
                        System.out.println("Pair of two Hori ColNo:"+colno+" Rowno: "+rowno);
                    }

            }
            else {
                if(arr[colno][rowno]==1 && arr[colno][rowno+1]==1)
                    if(cov[colno][rowno]==false || cov[colno][rowno+1]==false){
                        cov[colno][rowno]=true;
                        cov[colno][rowno+1]=true;
                        addOutput("HoriTwo",colno,rowno);
                        System.out.println("Pair of two Hori ColNo:"+colno+" Rowno: "+rowno);
                    }
            }
        }

    }
    public void groupofTwoVerti(int colno,int rowno)
    {
        if(colno==column-1){
            if(arr[colno][rowno]==1 && arr[0][rowno]==1)
                if(cov[colno][rowno]==false || cov[0][rowno]==false){
                    cov[colno][rowno]=true;
                    cov[0][rowno]=true;
                    addOutput("vertiTwo",colno,rowno);
                    System.out.println("Pair of two Verti ColNo:"+colno+" Rowno: "+rowno);
                }
        }
        else {
            if(arr[colno][rowno]==1 && arr[colno+1][rowno]==1)
                if(cov[colno][rowno]==false || cov[colno+1][rowno]==false){
                    cov[colno][rowno]=true;
                    cov[colno+1][rowno]=true;
                    addOutput("vertiTwo",colno,rowno);
                    System.out.println("Pair of two Veri ColNo:"+colno+" Rowno: "+rowno);
                }
        }
    }

    public void groupofOne(int colno,int rowno)
    {
        if(arr[colno][rowno]==1 )
            if(cov[colno][rowno]==false){
                cov[colno][rowno]=true;
                addOutput("one",colno,rowno);
                System.out.println("Pair of one ColNo:"+colno+" Rowno: "+rowno);
            }
    }

    public void printboolean(){
        System.out.println();
        for(int j=0;j<row;j++)
        {
            for(int q=0;q<column;q++){
                System.out.print("  "+arr[j][q]);
            }
            System.out.println();
        }
    }

    public void addOutput(String s,int colno,int rowno)
    {
        if(!pos && var==4){
            if(s.equals("groupofEightHori")){
                addTwoColumn(colno);
                output=output+" + ";
            }
            if(s.equals("groupofEightVerti")){
                addTwoRow(rowno);
                output=output+" + ";
            }
            if(s.equals("one")){
                addColumn(colno);
                addRow(rowno);
                output=output+" + ";
            }
            if(s.equals("vertiTwo")){
                addTwoColumn(colno);
                addRow(rowno);
                output=output+" + ";
            }
            if(s.equals("HoriTwo")){
                addColumn(colno);
                addTwoRow(rowno);
                output=output+" + ";
            }
            if(s.equals("HoriFour")){
                addColumn(colno);
                output=output+" + ";
            }
            if(s.equals("vertiFour")){
                addRow(rowno);
                output=output+" + ";
            }
            if(s.equals("groupfour")){
                addTwoColumn(colno);
                addTwoRow(rowno);
                output=output+" + ";
            }
        }
        if(pos && var==4){
            if(s.equals("groupofEightHori")){
                output=output+"(";
                addTwoColumnpos(colno);
                output=output+") . ";
            }
            if(s.equals("groupofEightVerti")){
                output=output+"(";
                addTwoRowpos(rowno);
                output=output+") . ";
            }
            if(s.equals("one")){
                output=output+"(";
                addColumnpos(colno);
                addRowpos(rowno);
                output=output+") . ";
            }
            if(s.equals("vertiTwo")){
                output=output+"(";
                addTwoColumnpos(colno);
                addRowpos(rowno);
                output=output+") . ";
            }
            if(s.equals("HoriTwo")){
                output=output+"(";
                addColumnpos(colno);
                addTwoRowpos(rowno);
                output=output+") . ";
            }
            if(s.equals("HoriFour")){
                output=output+"(";
                addColumnpos(colno);
                output=output+") . ";
            }
            if(s.equals("vertiFour")){
                output=output+"(";
                addRowpos(rowno);
                output=output+") . ";
            }
            if(s.equals("groupfour")){
                output=output+"(";
                addTwoColumnpos(colno);
                addTwoRowpos(rowno);
                output=output+") . ";
            }
        }
        if(!pos && var==3){
            if(s.equals("one")){
                addColumn(colno,3);
                addRow(rowno,3);
                output=output+" + ";
            }
            if(s.equals("vertiTwo")){
                addRow(rowno,3);
                output=output+" + ";
            }
            if(s.equals("HoriTwo")){
                addColumn(colno,3);
                addTwoRow(rowno,3);
                output=output+" + ";
            }
            if(s.equals("HoriFour")){
                addColumn(colno,3);
                output=output+" + ";
            }
            if(s.equals("groupfour")){
                addTwoRow(rowno,3);
                output=output+" + ";
            }
        }
        if(pos && var==3){
            if(s.equals("one")){
                output=output+"(";
                addColumnpos(colno,3);
                addRowpos(rowno,3);
                output=output+") . ";
            }
            if(s.equals("vertiTwo")){
                output=output+"(";
                addRowpos(rowno,3);
                output=output+") . ";
            }
            if(s.equals("HoriTwo")){
                output=output+"(";
                addColumnpos(colno,3);
                addTwoRowpos(rowno,3);
                output=output+") . ";
            }
            if(s.equals("HoriFour")){
                output=output+"(";
                addColumnpos(colno,3);
                output=output+") . ";
            }
            if(s.equals("groupfour")){
                output=output+"(";
                addTwoRowpos(rowno,3);
                output=output+") . ";
            }
        }
    }
    public void addColumn(int colno){
        switch(colno){
            case 0:output=output+"A'B'";
                break;
            case 1:output=output+"A'B";
                break;
            case 2:output=output+"AB";
                break;
            case 3:output=output+"AB'";
                break;
        }
    }
    public void addRow(int rowno){
        switch(rowno){
            case 0:output=output+"C'D'";
                break;
            case 1:output=output+"C'D";
                break;
            case 2:output=output+"CD";
                break;
            case 3:output=output+"CD'";
                break;
        }
    }
    public void addTwoColumn(int colno){
        switch(colno){
            case 0:output=output+"A'";
                break;
            case 1:output=output+"B";
                break;
            case 2:output=output+"A";
                break;
            case 3:output=output+"B'";
                break;
        }
    }
    public void addTwoRow(int rowno){
        switch(rowno){
            case 0:output=output+"C'";
                break;
            case 1:output=output+"D";
                break;
            case 2:output=output+"C";
                break;
            case 3:output=output+"D'";
                break;
        }
    }
    public void addColumn(int colno,int a){
        switch(colno){
            case 0:output=output+"A'";
                break;
            case 1:output=output+"A";
                break;
        }
    }
    public void addRow(int rowno,int a){
        switch(rowno){
            case 0:output=output+"B'C'";
                break;
            case 1:output=output+"B'C";
                break;
            case 2:output=output+"BC";
                break;
            case 3:output=output+"BC'";
                break;
        }
    }

    public void addTwoRow(int rowno,int a){
        switch(rowno){
            case 0:output=output+"B'";
                break;
            case 1:output=output+"C";
                break;
            case 2:output=output+"B";
                break;
            case 3:output=output+"C'";
                break;
        }
    }
    //pos 4 variable
    public void addColumnpos(int colno){
        switch(colno){
            case 0:output=output+"A+B+";
                break;
            case 1:output=output+"A+B'+";
                break;
            case 2:output=output+"A'+B'+";
                break;
            case 3:output=output+"A'+B+";
                break;
        }
    }
    public void addRowpos(int rowno){
        switch(rowno){
            case 0:output=output+"C+D";
                break;
            case 1:output=output+"C+D'";
                break;
            case 2:output=output+"C'+D'";
                break;
            case 3:output=output+"C'+D";
                break;
        }
    }
    public void addTwoColumnpos(int colno){
        switch(colno){
            case 0:output=output+"A+";
                break;
            case 1:output=output+"B'+";
                break;
            case 2:output=output+"A'+";
                break;
            case 3:output=output+"B+";
                break;
        }
    }
    public void addTwoRowpos(int rowno){
        switch(rowno){
            case 0:output=output+"C";
                break;
            case 1:output=output+"D'";
                break;
            case 2:output=output+"C'";
                break;
            case 3:output=output+"D";
                break;
        }
    }
    public void addColumnpos(int colno,int a){
        switch(colno){
            case 0:output=output+"A+";
                break;
            case 1:output=output+"A'+";
                break;
        }
    }
    public void addRowpos(int rowno,int a){
        switch(rowno){
            case 0:output=output+"B+C";
                break;
            case 1:output=output+"B+C'";
                break;
            case 2:output=output+"B'+C'";
                break;
            case 3:output=output+"B'+C";
                break;
        }
    }

    public void addTwoRowpos(int rowno,int a){
        switch(rowno){
            case 0:output=output+"B";
                break;
            case 1:output=output+"C'";
                break;
            case 2:output=output+"B'";
                break;
            case 3:output=output+"C";
                break;
        }
    }
    public String displayOutput()
    {
        String abc;
        if(output.length()==0)
            abc="Enter Equation";
        else
            abc=output.substring(0,output.length()-3);
        return abc;

    }
}
