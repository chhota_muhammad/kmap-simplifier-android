package com.example.chhota.kmapsimplifier;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView three,four;
    RadioButton sop,pos;
    ImageView solve,reset;
    int selected=4;
    Dialog openDialog;
    int value[][],value3[][]; boolean posboolean;
    ImageView button1,button2,button3,button4,button5,button6,button7,button8;
    ImageView button9,button10,button11,button12,button13,button14,button15,button16;

    TextView textView1,textView2,textView3,textView4,textView5,textView6,textView7,textView8;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initalize();
        initalizeVeriable();
        setOnClickListenerForAll();
    }
    private  void initalizeVeriable(){
        value=new int[4][4];
        value3=new int[2][4];
        posboolean=false;
    }
    private void initalize(){
        three=(ImageView)findViewById(R.id.three);
        four=(ImageView)findViewById(R.id.four);

        solve=(ImageView)findViewById(R.id.solve);
        reset=(ImageView)findViewById(R.id.reset);

        sop=(RadioButton)findViewById(R.id.sop);
        pos=(RadioButton)findViewById(R.id.pos);

        button1=(ImageView)findViewById(R.id.button1);
        button2=(ImageView)findViewById(R.id.button2);
        button3=(ImageView)findViewById(R.id.button3);
        button4=(ImageView)findViewById(R.id.button4);
        button5=(ImageView)findViewById(R.id.button5);
        button6=(ImageView)findViewById(R.id.button6);
        button7=(ImageView)findViewById(R.id.button7);
        button8=(ImageView)findViewById(R.id.button8);

        button9=(ImageView)findViewById(R.id.button9);
        button10=(ImageView)findViewById(R.id.button10);
        button11=(ImageView)findViewById(R.id.button11);
        button12=(ImageView)findViewById(R.id.button12);
        button13=(ImageView)findViewById(R.id.button13);
        button14=(ImageView)findViewById(R.id.button14);
        button15=(ImageView)findViewById(R.id.button15);
        button16=(ImageView)findViewById(R.id.button16);

        textView1=(TextView)findViewById(R.id.text1);
        textView2=(TextView)findViewById(R.id.text2);
        textView3=(TextView)findViewById(R.id.text3);
        textView4=(TextView)findViewById(R.id.text4);
        textView5=(TextView)findViewById(R.id.text5);
        textView6=(TextView)findViewById(R.id.text6);
        textView7=(TextView)findViewById(R.id.text7);
        textView8=(TextView)findViewById(R.id.text8);
    }

    private void setOnClickListenerForAll(){
        solve.setOnClickListener(this);
        reset.setOnClickListener(this);

        three.setOnClickListener(this);
        four.setOnClickListener(this);

        sop.setOnClickListener(this);
        pos.setOnClickListener(this);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);
        button10.setOnClickListener(this);
        button11.setOnClickListener(this);
        button12.setOnClickListener(this);
        button13.setOnClickListener(this);
        button14.setOnClickListener(this);
        button15.setOnClickListener(this);
        button16.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.three: threeClick(view);
                break;
            case R.id.four: fourClick(view);
                break;
            case R.id.sop: sopClick();
                break;
            case R.id.pos: posClick();
                break;
            case  R.id.button1:buttonClicked(1, view);
                break;
            case  R.id.button2:buttonClicked(2, view);
                break;
            case  R.id.button3:buttonClicked(3, view);
                break;
            case  R.id.button4:buttonClicked(4, view);
                break;
            case  R.id.button5:buttonClicked(5, view);
                break;
            case  R.id.button6:buttonClicked(6, view);
                break;
            case  R.id.button7:buttonClicked(7, view);
                break;
            case  R.id.button8:buttonClicked(8, view);
                break;
            case  R.id.button9:buttonClicked(9, view);
                break;
            case  R.id.button10:buttonClicked(10, view);
                break;
            case  R.id.button11:buttonClicked(11, view);
                break;
            case  R.id.button12:buttonClicked(12, view);
                break;
            case  R.id.button13:buttonClicked(13, view);
                break;
            case  R.id.button14:buttonClicked(14, view);
                break;
            case  R.id.button15:buttonClicked(15, view);
                break;
            case  R.id.button16:buttonClicked(16, view);
                break;
            case R.id.solve: solveButton();
                break;
            case R.id.reset: if(posboolean)
                resetButton(1);
                else
                resetButton(0);
                break;
            case R.id.ok: openDialog.dismiss();
                break;
        }
    }
    private  void resetButton(int zeroOne){
        for(int row=0;row<4;row++)
            for(int col=0;col<4;col++)
                value[row][col]=0;
        setZeroImage(zeroOne);
    }
    private  void setZeroImage(int zeroOne){
        if(zeroOne==0) {
            button1.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button2.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button3.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button4.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button5.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button6.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button7.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button8.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button9.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button10.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button11.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button12.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button13.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button14.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button15.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
            button16.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
        }
        else{
            button1.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button2.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button3.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button4.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button5.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button6.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button7.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button8.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button9.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button10.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button11.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button12.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button13.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button14.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button15.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
            button16.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
        }
    }
    private void solveButton(){
        String output="";
        LogicSimplification ls=new LogicSimplification();
        ls.arrayCopy(selected, 4, value, posboolean);
        ls.simplify();
        output=ls.displayOutput();
        openDialog = new Dialog(MainActivity.this);
        openDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        openDialog.setContentView(R.layout.show_answer);
        ImageView ok = (ImageView) openDialog.findViewById(R.id.ok);
        ok.setOnClickListener(this);
        TextView dialog_text = (TextView) openDialog.findViewById(R.id.dialog_text);
        dialog_text.setText(output);
        openDialog.show();
    }
    private void buttonClicked(int number,View view){
        switch (number){
            case 1:  value[0][0]++;
                value[0][0]%=2;
                setImaga(dis(value[0][0]), view);
                break;
            case 2:  value[0][1]++;
                value[0][1]%=2;
                setImaga(dis(value[0][1]), view);
                break;
            case 3:  value[0][2]++;
                value[0][2]%=2;
                setImaga(dis(value[0][2]), view);
                break;
            case 4:  value[0][3]++;
                value[0][3]%=2;
                setImaga(dis(value[0][3]), view);
                break;
            case 5:  value[1][0]++;
                value[1][0]%=2;
                setImaga(dis(value[1][0]), view);
                break;
            case 6:  value[1][1]++;
                value[1][1]%=2;
                setImaga(dis(value[1][1]), view);
                break;
            case 7:  value[1][2]++;
                value[1][2]%=2;
                setImaga(dis(value[1][2]), view);
                break;
            case 8:  value[1][3]++;
                value[1][3]%=2;
                setImaga(dis(value[1][3]), view);
                break;
            case 9:  value[2][0]++;
                value[2][0]%=2;
                setImaga(dis(value[2][0]), view);
                break;
            case 10:  value[2][1]++;
                value[2][1]%=2;
                setImaga(dis(value[2][1]), view);
                break;
            case 11:  value[2][2]++;
                value[2][2]%=2;
                setImaga(dis(value[2][2]), view);
                break;
            case 12:  value[2][3]++;
                value[2][3]%=2;
                setImaga(dis(value[2][3]), view);
                break;
            case 13:  value[3][0]++;
                value[3][0]%=2;
                setImaga(dis(value[3][0]), view);
                break;
            case 14:  value[3][1]++;
                value[3][1]%=2;
                setImaga(dis(value[3][1]), view);
                break;
            case 15:  value[3][2]++;
                value[3][2]%=2;
                setImaga(dis(value[3][2]), view);
                break;
            case 16:  value[3][3]++;
                value[3][3]%=2;
                setImaga(dis(value[3][3]), view);
                break;
        }
    }
    private  void setImaga(int result,View view){
        ImageView image=(ImageView)view;
        if(result==1){
            image.setImageDrawable(getResources().getDrawable(R.drawable.one_button));
        }
        else{
            image.setImageDrawable(getResources().getDrawable(R.drawable.zero_button));
        }
    }
    private  void sopClick(){
        posboolean=false;
        resetButton(0);
        if (selected==3)
            threeChangeText();
        else
            fourChangeText();
    }
    private  void posClick(){
        posboolean=true;
        resetButton(1);
        if (selected==3)
            threePosChangeText();
        else
            fourPosChangeText();
    }
    private void threeClick(View view){
        selected=3;posboolean=false;
        hideAndShowView(View.GONE);
        threeChangeText();
        three.setImageDrawable(getResources().getDrawable(R.drawable.threepselected));
        four.setImageDrawable(getResources().getDrawable(R.drawable.four));
        sop.setChecked(true);
        pos.setChecked(false);
    }

    private void fourClick(View view){
        selected=4;posboolean=false;
        hideAndShowView(view.VISIBLE);
        fourChangeText();
        four.setImageDrawable(getResources().getDrawable(R.drawable.fourselected));
        three.setImageDrawable(getResources().getDrawable(R.drawable.threep));
        sop.setChecked(true);
        pos.setChecked(false);
    }

    private void hideAndShowView(int visibility){
        textView7.setVisibility(visibility);
        textView8.setVisibility(visibility);

        button9.setVisibility(visibility);
        button10.setVisibility(visibility);
        button11.setVisibility(visibility);
        button12.setVisibility(visibility);
        button13.setVisibility(visibility);
        button14.setVisibility(visibility);
        button15.setVisibility(visibility);
        button16.setVisibility(visibility);
    }

    private void threeChangeText(){
        textView1.setText("B'C'");
        textView2.setText("B'C");
        textView3.setText("BC");
        textView4.setText("BC'");
        textView5.setText("A'");
        textView6.setText("A");
    }

    private void fourChangeText(){
        textView1.setText("C'D'");
        textView2.setText("C'D");
        textView3.setText("CD");
        textView4.setText("CD'");
        textView5.setText("A'B'");
        textView6.setText("A'B");
        textView7.setText("AB");
        textView8.setText("AB'");
    }

    private void threePosChangeText(){
        textView1.setText("B+C");
        textView2.setText("B+C'");
        textView3.setText("B'+C'");
        textView4.setText("B'+C'");
        textView5.setText("A");
        textView6.setText("A'");
    }

    private void fourPosChangeText(){
        textView1.setText("C+D");
        textView2.setText("C+D'");
        textView3.setText("C'+D'");
        textView4.setText("C'+D");
        textView5.setText("A+B");
        textView6.setText("A+B'");
        textView7.setText("A'+B'");
        textView8.setText("A'+B");
    }

    public int dis(int i){
        if(i==0)
            if(posboolean)
                return 1;
            else
                return 0;
        else
            if(posboolean)
                return 0;
            else
                return 1;
    }
}
